This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Built with

- React + Redux + Thunk
- Styled Components

## Tested with

- Jest
- Testing Library
- Redux Mock Store

## API

- [Rates API](https://ratesapi.io/)

## Live version

- [Revolut Currency Widget](https://revolut-exchange-currencies-app.maksakymenko.now.sh/)

## Run the app locally

- install all dependencies
```
yarn install
```
- run the application
```
yarn start
```
- run tests
```
yarn test
```
