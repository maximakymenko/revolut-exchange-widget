export function getCurrencyBalance(balances, currency) {
  return balances[currency] || 0;
}

export function isAmountValid(val) {
  return /^\d*(?:\.\d{0,2})?$/g.test(val);
}

export function parseAmount(val) {
  if (val.trim() === '.') {
    return 0;
  }
  return parseFloat(val);
}

export function formatAmount(val) {
  return (Math.round(val * 100) / 100).toString();
}
