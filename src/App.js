import React from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { GlobalStyle } from 'styles/global';
import { theme } from 'styles/theme';
import { store } from 'store'

import { ExchangeWidget } from 'components';

function App() {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <ExchangeWidget />
      </ThemeProvider>
    </Provider>
  );
}

export default App;
