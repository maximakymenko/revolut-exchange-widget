import { getCurrencyBalance } from 'helpers';
import { EXCHANGE_CURRENCY } from 'types';

export const exchangeCurrency = (inputCurrency, inputAmount, outputCurrency) => (dispatch, getState) => {
  const currentState = getState();

  const currencyData = currentState.rates[outputCurrency];

  if (!currencyData || !currencyData.isLoaded) {
    return;
  }

  const rate = currencyData.rates[inputCurrency];
  if (!rate) {
    return;
  }

  const balance = getCurrencyBalance(currentState.balance, inputCurrency);
  if (inputAmount > balance) {
    return;
  }

  return dispatch({
    payload: {
      inputAmount,
      outputAmount: inputAmount / rate,
      inputCurrency,
      outputCurrency,
    },
    type: EXCHANGE_CURRENCY,
  });
};
