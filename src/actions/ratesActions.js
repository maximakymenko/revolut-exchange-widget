import { FETCH_RATES_REQUEST, FETCH_RATES_SUCCESS, FETCH_RATES_ERROR } from 'types';
import { fetchCurrency } from 'services/api';

const fetchRatesRequest = currency => ({
  payload: { currency },
  type: FETCH_RATES_REQUEST,
});

const fetchRatesSuccess = (currency, response) => ({
  payload: { currency, response },
  type: FETCH_RATES_SUCCESS,
});


const fetchRatesError = error => ({
  payload: { error },
  type: FETCH_RATES_ERROR,
})

export const fetchRatesForCurrency = currency => async dispatch => {
  dispatch(fetchRatesRequest(currency));
  try {
    const response = await fetchCurrency(currency);
    dispatch(fetchRatesSuccess(currency, response));
  } catch (error) {
    dispatch(fetchRatesError(error));
  };
};
