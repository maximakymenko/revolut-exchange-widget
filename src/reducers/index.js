import { combineReducers } from 'redux'
import balanceReducer from './balanceReducer';
import ratesReducer from "./ratesReducer";

export default combineReducers({
  balance: balanceReducer,
  rates: ratesReducer,
})
