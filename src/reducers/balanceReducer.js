import { getCurrencyBalance } from 'helpers';
import { EXCHANGE_CURRENCY } from 'types';

const initialState = {
  'USD': 200,
  'EUR': 200,
  'GBP': 200,
}

const balanceReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case EXCHANGE_CURRENCY:
      return {
        ...state,
        [payload.inputCurrency]:
          getCurrencyBalance(state, payload.inputCurrency) - payload.inputAmount,
        [payload.outputCurrency]:
          getCurrencyBalance(state, payload.outputCurrency) + payload.outputAmount,
      };

    default:
      return state;
  }
}

export default balanceReducer;
