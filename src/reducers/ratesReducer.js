import { FETCH_RATES_REQUEST, FETCH_RATES_SUCCESS, FETCH_RATES_ERROR } from 'types';

const currencyReducer = (state = {}, { type, payload }) => {
  switch (type) {
    case FETCH_RATES_REQUEST: {
      const currencyState = state[payload.currency];
      return {
        ...state,
        [payload.currency]: {
          ...currencyState,
          isLoaded: false,
        },
      };
    }

    case FETCH_RATES_SUCCESS: {
      const currencyState = state[payload.currency];
      return {
        ...state,
        [payload.currency]: {
          ...currencyState,
          isLoaded: true,
          rates: payload.response.rates,
        },
      };
    }

    case FETCH_RATES_ERROR: {
      return {
        ...state,
        error: payload.error
      };
    }

    default:
      return state;
  }
}

export default currencyReducer;
