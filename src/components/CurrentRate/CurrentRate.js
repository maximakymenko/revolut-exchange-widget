import React from 'react';
import { objectOf, oneOfType, number, string } from 'prop-types';
import { formatAmount } from 'helpers';
import { Rate, RateContainer } from './CurrentRate.styled';

const CurrentRate = ({ currencySymbols, inputCurrency, outputCurrency, rate }) => {
  const invertedRate = 1 / rate;
  const formattedAmount = formatAmount(invertedRate);
  const input = `1${currencySymbols[inputCurrency]}`;
  const output = currencySymbols[outputCurrency];

  return (
    <RateContainer>
      <Rate data-testid="testCurrentRate">
        {input} = {formattedAmount}{output}
      </Rate>
    </RateContainer>
  )
}

CurrentRate.propTypes = {
  currencySymbols: objectOf(string).isRequired,
  inputCurrency: string.isRequired,
  outputCurrency: string.isRequired,
  rate: oneOfType([string, number]),
};

CurrentRate.defaultProps = {
  rate: 1,
};

export default CurrentRate;
