import styled from 'styled-components';

export const RateContainer = styled.div`
  display: flex;
  justify-content: center;
  position: absolute;
  top: calc(15rem  - 0.75rem); /* InputRow height - rate div height */
  left: 50%;
  transform: translateX(-50%);
`;

export const Rate = styled.div`
  display: inline-block;
  min-width: 6rem;
  text-align: center;
  font-size: 0.75rem;
  color: ${({ theme }) => theme.textBlue};
  background: white;
  padding: 0.25rem 1rem;
  border-radius: 20px;
  border: 2px solid ${({ theme }) => theme.lightGrey};
`;
