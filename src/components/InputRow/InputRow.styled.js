import styled from 'styled-components';

export const Row = styled.div`
  display: flex;
  align-items: center;
  height: 15rem;
  padding: 2rem;
  background: ${({ output, theme }) => output && theme.lightGrey};
`;

export const Select = styled.select`
  border: 0;
  font-size: 3rem;
  background: ${({ arrowImg }) => `url(${arrowImg}) 96% no-repeat transparent`};
  background-size: 2rem;
  outline: 0;
  appearance: none;
  min-width: 8rem;
  cursor: pointer;

  @media (max-width: ${({ theme }) => theme.mobile}) {
    padding: 0;
    font-size: 2rem;
  }
`;

export const Input = styled.input`
  caret-color: ${({ theme }) => theme.textBlue};
  font-size: 3rem;
  background: transparent;
  width: 60%;
  outline: none;
  border: 0;
  text-align: right;

  @media (max-width: ${({ theme }) => theme.mobile}) {
    font-size: 2rem;
  }

  &::placeholder {
    color: ${({ theme }) => theme.spaceGrey};
  }
`;

export const Balance = styled.div`
  font-size: 0.8rem;
  font-weight: 300;
  margin-top: 0.5rem;
  color: ${({ error, theme }) => error ? theme.primaryRed : theme.spaceGrey};
  transition: color 0.3s linear;
`;
