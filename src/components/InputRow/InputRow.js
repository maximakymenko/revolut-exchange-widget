import React from 'react';
import { bool, objectOf, string, number, func } from 'prop-types';
import { formatAmount } from 'helpers';
import arrowDown from 'assets/icons/ic_arrow_down.svg';
import { Balance, Input, Row, Select } from './InputRow.styled';

const InputRow = ({
  currency,
  onCurrencyChange,
  availableCurrencies,
  amount,
  onAmountChange,
  currencySymbols,
  balance,
  output,
  notEnoughBalance,
  shouldHideInput,
}) => {
  return (
    <Row output={output} data-testid="inputRow">
      <div>
        <Select
          arrowImg={arrowDown}
          value={currency}
          data-testid="testSelect"
          onChange={e => onCurrencyChange(e.target.value)}
        >
          {Object.values(availableCurrencies).map(currency => (
            <option
              key={currency}
              value={currency}>
              {currency}
            </option>
          ))}
        </Select>
        <Balance error={notEnoughBalance} data-testid="testBalance">
          Balance: {formatAmount(balance)}{' '}{currencySymbols[currency]}
        </Balance>
      </div>
      {!shouldHideInput &&
        <Input
          placeholder="0"
          data-testid="testInput"
          value={amount}
          onChange={e => onAmountChange(e.target.value)}
          type="text"
        />
      }
    </Row>
  )
}

InputRow.propTypes = {
  currency: string.isRequired,
  onCurrencyChange: func.isRequired,
  onAmountChange: func.isRequired,
  availableCurrencies: objectOf(string).isRequired,
  amount: string.isRequired,
  balance: number.isRequired,
  currencySymbols: objectOf(string).isRequired,
  output: bool,
  shouldHideInput: bool,
  notEnoughBalance: bool,
};

InputRow.defaultProps = {
  shouldHideInput: false,
  output: false,
  notEnoughBalance: false,
}

export default InputRow;
