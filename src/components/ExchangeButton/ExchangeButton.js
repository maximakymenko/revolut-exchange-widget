import React from 'react';
import { bool, func, string } from 'prop-types';
import { StyledButton } from './ExchangeButton.styled';

const ExchangeButton = ({ disabled, onClick, title }) => {
  return (
    <StyledButton
      type="button"
      data-testid="testExchangeButton"
      disabled={disabled}
      onClick={onClick}>
      {title}
    </StyledButton>
  )
}

ExchangeButton.propTypes = {
  disabled: bool,
  title: string.isRequired,
  onClick: func.isRequired,
};

ExchangeButton.defaultProps = {
  disabled: false,
};

export default ExchangeButton;
