import styled from 'styled-components';

export const StyledButton = styled.button`
  background: ${({ theme }) => theme.primaryPink};
  border-radius: 32px;
  border: 1px solid transparent;
  color: white;
  outline: 0;
  box-shadow: 0 0 0 1px ${({ theme }) => theme.primaryPink};;
  font-size: 1rem;
  display: block;
  margin: 2rem auto;
  padding: 0.75rem 2.5rem;
  cursor: pointer;
  transition: background 0.3s linear, box-shadow 0.3s linear;

  @media (max-width: ${({ theme }) => theme.mobile}) {
    margin-top: 3rem;
  }

  &:hover {
    background: ${({ theme }) => theme.darkPink};
    box-shadow: 0 0 0 1px ${({ theme }) => theme.darkPink};
  }

  &:disabled {
    cursor: not-allowed;
    background: ${({ theme }) => theme.lightPink};
    box-shadow: 0 0 0 1px ${({ theme }) => theme.lightPink};
  }
`;
