import styled from 'styled-components';

export const StyledExchangeWidget = styled.div`
  background: white;
  height: 38rem;
  width: 24rem;
  border-radius: 0.3rem;
  position: relative;

  @media (max-width: ${({ theme }) => theme.mobile}) {
    height: 100vh;
    width: 100vw;
    margin: 0;
  }
`;

export const Error = styled.span`
  display: block;
  text-align: center;
  font-weight: 700;
  margin-bottom: 1rem;
  color: white;
`;
