import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { exchangeCurrency } from 'actions/balanceActions'
import { fetchRatesForCurrency } from 'actions/ratesActions';
import { AVAILABLE_CURRENCIES, CURRENCY_SYMBOLS } from 'constants/currency';
import { formatAmount, getCurrencyBalance, isAmountValid, parseAmount } from 'helpers';
import { usePrevious } from 'hooks';
import { CurrencySwitcher, CurrentRate, InputRow, ExchangeButton } from 'components';
import { Error, StyledExchangeWidget } from './ExchangeWidget.styled';

const POLLING_INTERVAL = 10 * 1000;
const ENTER_KEY_CODE = 13;

const ExchangeWidget = () => {
  const [mainCurrency, subCurrency] = Object.values(AVAILABLE_CURRENCIES);

  const [inputAmount, setInputAmount] = useState('');
  const [outputAmount, setOutputAmount] = useState('');
  const [inputCurrency, setInputCurrency] = useState(mainCurrency);
  const [outputCurrency, setOutputCurrency] = useState(subCurrency);

  const balance = useSelector(state => state.balance);
  const rates = useSelector(state => state.rates);
  const error = useSelector(state => state.rates.error);
  const dispatch = useDispatch();

  const inputBalance = getCurrencyBalance(balance, inputCurrency);
  const outputBalance = getCurrencyBalance(balance, outputCurrency);

  const getRate = useCallback((inputCurrency, outputCurrency) => {
    const currencyData = rates[outputCurrency];
    return (currencyData && currencyData.isLoaded && currencyData.rates[inputCurrency]) || undefined;
  }, [rates]);

  const convertAmount = useCallback((inputCurrency, inputAmount, outputCurrency, isInversed) => {
    let amount = '';
    if (isAmountValid(inputAmount)) {
      const parsedAmount = parseAmount(inputAmount);

      if (!isNaN(parsedAmount)) {
        const rate = getRate(inputCurrency, outputCurrency);

        if (rate) {
          const outputAmount = isInversed ?
            parsedAmount * rate :
            parsedAmount / rate;
          amount = formatAmount(outputAmount);
        }
      }
    };

    return amount;
  }, [getRate]);

  const onInputAmountChange = useCallback(inputAmount => {
    setInputAmount(inputAmount);
    setOutputAmount(convertAmount(inputCurrency, inputAmount, outputCurrency))
  }, [convertAmount, inputCurrency, outputCurrency]);

  const onOutputAmountChange = outputAmount => {
    setInputAmount(convertAmount(inputCurrency, outputAmount, outputCurrency, true));
    setOutputAmount(outputAmount);
  };

  const onEnterPressed = e => {
    if (e.keyCode === ENTER_KEY_CODE) {
      exchangeCurrencies();
    }
  };

  useEffect(() => {
    document.addEventListener('keydown', onEnterPressed);
    return () => document.removeEventListener('keydown', onEnterPressed);
  });

  // we create custom hook to get previous state
  const prevRates = usePrevious(rates)
  // to update input, when we choose rate, that
  // hasn't been loaded yet
  useEffect(() => {
    if (prevRates !== rates) {
      onInputAmountChange(inputAmount);
    }
  }, [inputAmount, onInputAmountChange, prevRates, rates]);

  useEffect(() => {
    const fetchRatesWithInterval = setInterval(() => dispatch(fetchRatesForCurrency(outputCurrency)), POLLING_INTERVAL);
    dispatch(fetchRatesForCurrency(outputCurrency));
    return () => clearInterval(fetchRatesWithInterval);
  }, [dispatch, outputCurrency]);

  const onInputCurrencyChange = currency => {
    setOutputAmount(convertAmount(currency, inputAmount, outputCurrency));
    setInputCurrency(currency);
  };

  const onOutputCurrencyChange = currency => {
    setOutputAmount(convertAmount(inputCurrency, inputAmount, currency));
    setOutputCurrency(currency);
  };

  const isNotEnoughBalance = () => {
    const parsedInputAmount = parseAmount(inputAmount);
    const InputBalance = getCurrencyBalance(balance, inputCurrency);

    return !isNaN(parsedInputAmount) && InputBalance < parsedInputAmount;
  };

  const isExchangeDisabled = () => {
    const parsedInputAmount = parseAmount(inputAmount);
    const parsedOutputAmount = parseAmount(outputAmount);

    return (
      isNaN(parsedInputAmount) ||
      isNaN(parsedOutputAmount) ||
      parsedInputAmount < 0 ||
      inputCurrency === outputCurrency ||
      isNotEnoughBalance()
    );
  };

  const exchangeCurrencies = () => {
    if (isExchangeDisabled()) {
      return;
    }

    const parsedInputAmount = parseAmount(inputAmount);

    dispatch(exchangeCurrency(inputCurrency, parsedInputAmount, outputCurrency));
    setInputAmount('');
    setOutputAmount('');
  };

  const switchCurrencies = () => {
    setInputCurrency(outputCurrency);
    setOutputCurrency(inputCurrency);
  }

  return (
    <>
      {error && <Error>There is an error occured, please try again later.</Error>}
      <StyledExchangeWidget>
        <InputRow
          currency={inputCurrency}
          onCurrencyChange={onInputCurrencyChange}
          onAmountChange={onInputAmountChange}
          availableCurrencies={AVAILABLE_CURRENCIES}
          amount={inputAmount}
          currencySymbols={CURRENCY_SYMBOLS}
          notEnoughBalance={isNotEnoughBalance()}
          balance={inputBalance}
        />

        <CurrencySwitcher onClick={switchCurrencies} />

        <CurrentRate
          currencySymbols={CURRENCY_SYMBOLS}
          inputCurrency={inputCurrency}
          outputCurrency={outputCurrency}
          rate={getRate(inputCurrency, outputCurrency)}
        />

        <InputRow
          shouldHideInput={inputCurrency === outputCurrency}
          currency={outputCurrency}
          onCurrencyChange={onOutputCurrencyChange}
          onAmountChange={onOutputAmountChange}
          availableCurrencies={AVAILABLE_CURRENCIES}
          amount={outputAmount}
          currencySymbols={CURRENCY_SYMBOLS}
          balance={outputBalance}
          output
        />

        <ExchangeButton
          title="Exchange"
          disabled={isExchangeDisabled()}
          onClick={exchangeCurrencies}
        />
      </StyledExchangeWidget>
    </>
  )
};

export default ExchangeWidget;
