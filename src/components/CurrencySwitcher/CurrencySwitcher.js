import React from 'react';
import { func } from 'prop-types';
import { Switcher } from './CurrencySwitcher.styled';
import switchIcon from 'assets/icons/ic_switch_icon.svg';

const CurrencySwitcher = ({ onClick }) => {
  return (
    <Switcher onClick={onClick} data-testid="testSwitchButton">
      <img src={switchIcon} alt="Switch icon" />
    </Switcher>
  )
};

CurrencySwitcher.propTypes = {
  onClick: func.isRequired,
};

export default CurrencySwitcher;
