import styled from 'styled-components';

export const Switcher = styled.button`
  position: absolute;
  top: calc(15rem  - 0.75rem); /* InputRow height - rate div height */
  left: 10%;
  background: white;
  border-radius: 50%;
  border: 2px solid ${({ theme }) => theme.lightGrey};
  outline: none;
  cursor: pointer;

  img {
    width: 0.75rem;
    height: 1.25rem;
  }
`;
