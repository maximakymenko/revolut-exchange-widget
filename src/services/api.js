import { API_URL } from 'constants/api';

export const fetchCurrency = async currency => {
  try {
    const response = await fetch(`${API_URL}?base=${currency}`);
    return await response.json();
  } catch (err) {
    console.error(err);
  }
};
