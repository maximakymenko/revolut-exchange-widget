import React from 'react';
import { render } from '@testing-library/react';
import { CURRENCY_SYMBOLS } from 'constants/currency';
import { CurrentRate } from 'components';

describe('<CurrentRate />', () => {
  it('should render the component and match snapshot', () => {
    const container = render(
      <CurrentRate
        currencySymbols={CURRENCY_SYMBOLS}
        inputCurrency="EUR"
        outputCurrency="USD"
        rate="1.12"
      />
    );

    expect(container).not.toBeNull();
    expect(container.firstChild).toMatchSnapshot();
  })

  it('should render proper values and currencies', () => {
    const { getByTestId } = render(
      <CurrentRate
        currencySymbols={CURRENCY_SYMBOLS}
        inputCurrency="USD"
        outputCurrency="EUR"
        rate="1.12"
      />
    );

    expect(getByTestId('testCurrentRate').textContent).toContain('1$ = 0.89€')
  })
});
