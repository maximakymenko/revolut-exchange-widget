import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { InputRow } from 'components';
import { AVAILABLE_CURRENCIES, CURRENCY_SYMBOLS } from 'constants/currency';

describe('<InputRow />', () => {
  let context;
  const onCurrencyChange = jest.fn();
  const onAmountChange = jest.fn();

  beforeEach(() => {
    const { container, getByTestId } = render(
      <InputRow
        currency="USD"
        onCurrencyChange={onCurrencyChange}
        onAmountChange={onAmountChange}
        availableCurrencies={AVAILABLE_CURRENCIES}
        amount="100"
        testId="testInputRow"
        currencySymbols={CURRENCY_SYMBOLS}
        balance={150}
      />
    );

    context = {
      component: () => container,
      balanceEl: () => getByTestId('testBalance'),
      inputEl: () => getByTestId('testInput'),
      selectEl: () => getByTestId('testSelect'),
    }
  })

  it('should render the component and match snapshot', () => {
    const { component } = context;

    expect(component).not.toBeNull();
    expect(component.firstChild).toMatchSnapshot();
  });

  it('should show proper balance', () => {
    const { balanceEl } = context;

    expect(balanceEl().textContent).toBe('Balance: 150 $')
  });

  it('should fire onChange event on amount change', () => {
    const { inputEl } = context;
    fireEvent.change(inputEl(), { target: { value: '111' } });

    expect(onAmountChange).toHaveBeenCalledTimes(1);
  });

  it('should fire onChange event on currency change', () => {
    const { selectEl } = context;
    fireEvent.change(selectEl(), { target: { value: 'EUR' } });

    expect(onCurrencyChange).toHaveBeenCalledTimes(1);
  });
});
