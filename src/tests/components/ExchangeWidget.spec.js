import React from 'react'
import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react'
import { ExchangeWidget } from 'components';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('<ExchangeWidget />', () => {
  let context;

  beforeEach(() => {
    const store = mockStore({
      balance: {},
      rates: {},
      error: null,
    });

    const { container, getAllByTestId, getByTestId } = render(
      <Provider store={store}>
        <ExchangeWidget />
      </Provider>
    );

    context = {
      component: () => container,
      currentRate: () => getByTestId('testCurrentRate'),
      exchangeButton: () => getByTestId('testExchangeButton'),
      switchButton: () => getByTestId('testSwitchButton'),
      inputRows: () => getAllByTestId('inputRow'),
    }
  });

  it('should render the component and match snapshot', () => {
    const { component } = context;

    expect(component()).not.toBeNull();
    expect(component()).toMatchSnapshot();
  });

  it('should render all components ', () => {
    expect(context.currentRate()).toBeVisible();
    expect(context.exchangeButton()).toBeVisible();
    expect(context.switchButton()).toBeVisible();
    // because we have two input rows
    context.inputRows().forEach(el => expect(el).toBeVisible());
  });
});
