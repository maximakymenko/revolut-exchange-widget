import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { CurrencySwitcher } from 'components';

describe('<CurrencySwitcher />', () => {
  const onClick = jest.fn();

  it('should render the component and match snapshot', () => {
    const container = render(<CurrencySwitcher onClick={onClick} />);

    expect(container).not.toBeNull();
    expect(container.firstChild).toMatchSnapshot();
  })

  it('should fire onClick function', () => {
    const { getByTestId } = render(<CurrencySwitcher onClick={onClick} />);
    fireEvent.click(getByTestId('testSwitchButton'));

    expect(onClick).toHaveBeenCalledTimes(1);
  })
});
