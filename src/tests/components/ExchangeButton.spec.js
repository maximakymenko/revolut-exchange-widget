import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import { ExchangeButton } from 'components';

describe('<ExchangeButton />', () => {
  const onClick = jest.fn();

  it('should render the component and match snapshot', () => {
    const container = render(
      <ExchangeButton
        title="test"
        onClick={onClick}
      />
    );

    expect(container).not.toBeNull();
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should contain proper title', () => {
    const { getByTestId } = render(
      <ExchangeButton
        title="test title"
        onClick={onClick}
      />
    );
    expect(getByTestId('testExchangeButton').textContent).toBe('test title');
  });

  it('should not fire onClick when disabled', () => {
    const { getByTestId } = render(
      <ExchangeButton
        title="test title"
        disabled
        onClick={onClick}
      />
    );
    fireEvent.click(getByTestId('testExchangeButton'));
    expect(onClick).toHaveBeenCalledTimes(0);
  });

  it('should fire onClick when is not disabled', () => {
    const { getByTestId } = render(
      <ExchangeButton
        title="test title"
        onClick={onClick}
      />
    );
    fireEvent.click(getByTestId('testExchangeButton'));
    expect(onClick).toHaveBeenCalledTimes(1);
  });
});
