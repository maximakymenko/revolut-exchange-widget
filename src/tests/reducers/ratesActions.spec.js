import { AVAILABLE_CURRENCIES } from 'constants/currency';
import { FETCH_RATES_REQUEST, FETCH_RATES_SUCCESS, FETCH_RATES_ERROR } from 'types';
import ratesReducer from 'reducers/ratesReducer';

describe('Rates reducer', () => {
  it('should return initial state', () => {
    expect(ratesReducer(undefined, {})).toEqual({});
  });

  it('should handle the FETCH_RATES_REQUEST action', () => {
    expect(ratesReducer(
      {
        [AVAILABLE_CURRENCIES.USD]: {
          isLoaded: false,
        },
      },
      {
        payload: {
          currency: AVAILABLE_CURRENCIES.USD,
        },
        type: FETCH_RATES_REQUEST,
      },
    )).toEqual({
      [AVAILABLE_CURRENCIES.USD]: {
        isLoaded: false,
      },
    });
  });

  it('should handle the FETCH_RATES_SUCCESS action', () => {
    expect(ratesReducer(
      {
        [AVAILABLE_CURRENCIES.USD]: {
          isLoaded: true,
        },
      },
      {
        payload: {
          currency: AVAILABLE_CURRENCIES.USD,
          response: {
            base: AVAILABLE_CURRENCIES.USD,
            date: '2019-11-29',
            rates: {
              [AVAILABLE_CURRENCIES.EUR]: 0.8,
            },
          },
        },
        type: FETCH_RATES_SUCCESS,
      },
    )).toEqual({
      [AVAILABLE_CURRENCIES.USD]: {
        isLoaded: true,
        rates: {
          [AVAILABLE_CURRENCIES.EUR]: 0.8,
        },
      },
    });
  });

  it('should handle the FETCH_RATES_ERROR action', () => {
    expect(ratesReducer(
      {
        [AVAILABLE_CURRENCIES.USD]: {
          isLoaded: true,
        },
      },
      {
        payload: { error: 'test error' },
        type: FETCH_RATES_ERROR,
      },
    )).toEqual({
      [AVAILABLE_CURRENCIES.USD]: {
        isLoaded: true,
      },
      error: 'test error',
    });
  });
});
