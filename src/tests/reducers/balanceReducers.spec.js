import { AVAILABLE_CURRENCIES } from 'constants/currency';
import { EXCHANGE_CURRENCY } from 'types';
import balanceReducer from 'reducers/balanceReducer';

describe('Balance reducer', () => {
  it('should return initial state', () => {
    expect(balanceReducer(undefined, {})).toEqual({
      [AVAILABLE_CURRENCIES.EUR]: 200,
      [AVAILABLE_CURRENCIES.GBP]: 200,
      [AVAILABLE_CURRENCIES.USD]: 200,
    });
  });

  it('should handle EXCHANGE_CURRENCY action', () => {
    expect(balanceReducer(
      {
        [AVAILABLE_CURRENCIES.GBP]: 200,
      },
      {
        payload: {
          inputAmount: 50,
          outputAmount: 50,
          inputCurrency: AVAILABLE_CURRENCIES.GBP,
          outputCurrency: AVAILABLE_CURRENCIES.EUR,
        },
        type: EXCHANGE_CURRENCY,
      },
    )).toEqual({
      [AVAILABLE_CURRENCIES.GBP]: 150,
      [AVAILABLE_CURRENCIES.EUR]: 50,
    });

    expect(balanceReducer(
      {
        [AVAILABLE_CURRENCIES.USD]: 200,
        [AVAILABLE_CURRENCIES.GBP]: 100,
      },
      {
        payload: {
          inputAmount: 50,
          outputAmount: 50,
          inputCurrency: AVAILABLE_CURRENCIES.GBP,
          outputCurrency: AVAILABLE_CURRENCIES.USD,
        },
        type: EXCHANGE_CURRENCY,
      },
    )).toEqual({
      [AVAILABLE_CURRENCIES.USD]: 250,
      [AVAILABLE_CURRENCIES.GBP]: 50,
    });
  });
});
