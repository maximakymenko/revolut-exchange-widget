import { AVAILABLE_CURRENCIES } from 'constants/currency';
import { getCurrencyBalance, isAmountValid, formatAmount, parseAmount } from 'helpers';

describe('getCurrencyBalance', () => {
  it('returns the right balance', () => {
    const balance = {
      [AVAILABLE_CURRENCIES.USD]: 100,
      [AVAILABLE_CURRENCIES.EUR]: 50,
      [AVAILABLE_CURRENCIES.GBP]: 75,
    };

    expect(getCurrencyBalance(balance, AVAILABLE_CURRENCIES.USD)).toEqual(100);
    expect(getCurrencyBalance(balance, AVAILABLE_CURRENCIES.EUR)).toEqual(50);
    expect(getCurrencyBalance(balance, AVAILABLE_CURRENCIES.GBP)).toEqual(75);
  });

  it('should return zero if there are no balance for currency', () => {
    const balance = {
      [AVAILABLE_CURRENCIES.USD]: 10,
    };

    expect(getCurrencyBalance(balance, AVAILABLE_CURRENCIES.EUR)).toEqual(0);
  });
});

describe('isAmountValid', () => {
  it('should return true if amount is valid', () => {
    expect(isAmountValid('5')).toEqual(true);
    expect(isAmountValid('.5')).toEqual(true);
    expect(isAmountValid('5.')).toEqual(true);
    expect(isAmountValid('5.55')).toEqual(true);
  });

  it('should return false if value is broken', () => {
    expect(isAmountValid('1.123')).toEqual(false);
    expect(isAmountValid('bla-bla')).toEqual(false);
    expect(isAmountValid('!422')).toEqual(false);
    expect(isAmountValid('1,12')).toEqual(false);
  });
});

describe('parseAmount', () => {
  it('should return 0 if value is "."', () => {
    expect(parseAmount('.')).toEqual(0);
  });

  it('should parse other values', () => {
    expect(parseAmount('12.23')).toEqual(parseFloat('12.23'));
    expect(parseAmount('bla-bla')).toEqual(parseFloat('bla-bla'));
  })
});

describe('formatAmount', () => {
  it('should format values correctly', () => {
    expect(formatAmount(2.3)).toEqual('2.3');
    expect(formatAmount(2.30023)).toEqual('2.3');
    expect(formatAmount(1.002233)).toEqual('1');
  });
});






