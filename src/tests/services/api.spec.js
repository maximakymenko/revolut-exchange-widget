import { API_URL } from 'constants/api';
import { fetchCurrency } from 'services/api';
import { AVAILABLE_CURRENCIES } from 'constants/currency';

describe('fetchCurrency', () => {
  it('should call the API and fetch the data for the right currency', async () => {
    const mockedPromise = Promise.resolve({ json: () => undefined });

    jest.spyOn(global, 'fetch').mockImplementation(() => mockedPromise);

    await fetchCurrency(AVAILABLE_CURRENCIES.EUR);
    expect(global.fetch).toHaveBeenCalledTimes(1);
    expect(global.fetch).toHaveBeenLastCalledWith(`${API_URL}?base=${AVAILABLE_CURRENCIES.EUR}`);

    await fetchCurrency(AVAILABLE_CURRENCIES.USD);
    expect(global.fetch).toHaveBeenCalledTimes(2);
    expect(global.fetch)
      .toHaveBeenLastCalledWith(`${API_URL}?base=${AVAILABLE_CURRENCIES.USD}`);

    await fetchCurrency(AVAILABLE_CURRENCIES.GBP);
    expect(global.fetch).toHaveBeenCalledTimes(3);
    expect(global.fetch)
      .toHaveBeenLastCalledWith(`${API_URL}?base=${AVAILABLE_CURRENCIES.GBP}`);

    global.fetch.mockRestore();
  });
});
