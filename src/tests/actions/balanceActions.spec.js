import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { AVAILABLE_CURRENCIES } from 'constants/currency';
import { EXCHANGE_CURRENCY } from 'types';
import { exchangeCurrency } from 'actions/balanceActions';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('Balance actions', () => {
  it('should call EXCHANGE_CURRENCY action with correct data', () => {
    const storeState = {
      balance: {
        [AVAILABLE_CURRENCIES.EUR]: 10,
        [AVAILABLE_CURRENCIES.USD]: 0,
      },
      rates: {
        [AVAILABLE_CURRENCIES.USD]: {
          isLoaded: true,
          rates: { [AVAILABLE_CURRENCIES.EUR]: 2 },
        },
      },
    };
    const store = mockStore(storeState);

    store.dispatch(exchangeCurrency(AVAILABLE_CURRENCIES.EUR, 2, AVAILABLE_CURRENCIES.USD));
    expect(store.getActions()).toEqual([
      {
        payload: {
          inputAmount: 2,
          outputAmount: 1,
          inputCurrency: AVAILABLE_CURRENCIES.EUR,
          outputCurrency: AVAILABLE_CURRENCIES.USD,
        },
        type: EXCHANGE_CURRENCY,
      },
    ]);
  });

  it('should not dispatch any action if there are no requested currency rate', () => {
    const storeState = {
      balance: {
        [AVAILABLE_CURRENCIES.USD]: 10,
        [AVAILABLE_CURRENCIES.EUR]: 0,
      },
      rates: {
        [AVAILABLE_CURRENCIES.EUR]: {
          isLoaded: true,
          rates: {},
        },
      },
    };
    const store = mockStore(storeState);

    store.dispatch(exchangeCurrency(AVAILABLE_CURRENCIES.USD, 2, AVAILABLE_CURRENCIES.EUR));
    expect(store.getActions().length).toEqual(0);
  });

  it('should not dispatch any action if there are no currency rates at all', () => {
    const storeState = {
      balance: {
        [AVAILABLE_CURRENCIES.USD]: 10,
        [AVAILABLE_CURRENCIES.EUR]: 0,
      },
      rates: {},
    };
    const store = mockStore(storeState);

    store.dispatch(exchangeCurrency(AVAILABLE_CURRENCIES.USD, 2, AVAILABLE_CURRENCIES.EUR));
    expect(store.getActions().length).toEqual(0);
  });

  it('should not dispatch an action if not enough source balance', () => {
    const storeState = {
      balance: {
        [AVAILABLE_CURRENCIES.USD]: 1,
        [AVAILABLE_CURRENCIES.EUR]: 0,
      },
      rates: {
        [AVAILABLE_CURRENCIES.EUR]: {
          isLoaded: true,
          rates: { [AVAILABLE_CURRENCIES.USD]: 2 },
        },
      },
    };
    const store = mockStore(storeState);

    store.dispatch(exchangeCurrency(AVAILABLE_CURRENCIES.USD, 2, AVAILABLE_CURRENCIES.EUR));
    expect(store.getActions().length).toEqual(0);
  });
});
