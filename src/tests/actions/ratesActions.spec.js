import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { FETCH_RATES_REQUEST, FETCH_RATES_SUCCESS } from 'types';
import { fetchRatesForCurrency } from 'actions/ratesActions';
import { AVAILABLE_CURRENCIES } from 'constants/currency';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('Rates actions', () => {
  it('should call action on request success', async () => {
    const testCurrency = AVAILABLE_CURRENCIES.EUR;
    const mockedPromise = Promise.resolve({ json: () => undefined });

    jest.spyOn(global, 'fetch').mockImplementation(() => mockedPromise);

    const expectedActions = [
      { payload: { currency: testCurrency }, type: FETCH_RATES_REQUEST },
      {
        payload: { currency: testCurrency, response: undefined },
        type: FETCH_RATES_SUCCESS,
      },
    ];
    const store = mockStore({ balance: {}, rates: {} });

    await store.dispatch(fetchRatesForCurrency(testCurrency));
    expect(store.getActions()).toEqual(expectedActions);

    global.fetch.mockRestore();
  });
});
