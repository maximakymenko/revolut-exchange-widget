export const theme = {
  textBlue: '#0C80EF',
  spaceGrey: '#A5AEB6',
  lightGrey: '#F0F3F5',
  primaryRed: '#C10D0D',
  primaryPink: '#EB008D',
  darkPink: '#BD0172',
  lightPink: '#FFA2DA',
  bgTop: '#0C80EF',
  bgBottom: '#0FA5F0',

  mobile: '576px',
}
