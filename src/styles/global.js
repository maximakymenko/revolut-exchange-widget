import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  html, body {
    margin: 0;
    padding: 0;
  }

  *, *::after, *::before {
    box-sizing: border-box;
  }

  body {
    font-family: sans-serif;
    background: ${({ theme }) => `linear-gradient(to left,  ${theme.bgTop} 0%, ${theme.bgBottom} 100%)`};
    display: flex;
    height: 100vh;
    justify-content: center;
    align-items: center;
  }
`;
